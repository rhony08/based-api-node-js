# Simple REST API

A simple REST API. MVP - Node (Express)

## Install all modules
>>>
    npm install
>>>

## Run app
### Run via terminal
>>>
    docker-compose up
>>>
After the app running, execute create table (check the query from .sql files). Default root password and db name can be check on .env files. For checking container name you can run `docker ps` and check name of sql db. 
>>>
    docker exec -it <container name> bash
    mysql -u root -p
    use <db_name>
>>>

Please consider to create separated user for Read-Only and Read-Write access. If you do it, you can adjust user and password of the user at .env files.

## How to Add new API

- add new function for handler on Controller
- add new endpoint to router/index.js
- rerun the app

## How to Add new Controller:
- Duplicate on of existing controller & rename it
- Register the controller at controller/index.js

## How to Run Testing:
### Unit Test
- If you want to run unit test, you can run `npm unit_test` or `yarn unit_test`
- If you want to run API test, you can run `npm api_test` or `yarn api_test`
- And if you want to run integration test, please run the app first with command `docker-compose up` (make sure you already create the database and table), then you can run `npm integration_test` or `yarn integration_test`. *note: integration test for add only success for first running, since the date is unique (and does not have delete API), you can uncomment if want to run it.