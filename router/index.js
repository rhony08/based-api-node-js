const {
  WeightController,
} = require('../controller');

module.exports = (app) => {
  
  app.get('/weight', WeightController.listWeight);
  app.post('/weight', WeightController.createWeight);
  app.put('/weight/:date', WeightController.updateWeight);
};
  