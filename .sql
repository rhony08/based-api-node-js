CREATE TABLE `weights` (
  `date_input` date NOT NULL UNIQUE,
  `max` int NOT NULL,
  `min` int NOT NULL,
  `created_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`date_input`)
);