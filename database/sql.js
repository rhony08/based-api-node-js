const mysql = require('mysql');

class SQLDatabaseConnection {
    constructor(poolRO, poolRW) {
        this.poolRO = poolRO;
        this.poolRW = poolRW;
    }

    setConnection(host, user, pwd, db, limit, type) {
        let conn = mysql.createPool({
            connectionLimit : limit,
            host: host,
            user: user,
            password: pwd,
            database: db
        });
        if (type === "RO") {
            this.poolRO = conn;
        } else {
            this.poolRW = conn;
        }
    }

    async findData(sqlQuery, values) {
        return await new Promise((resolve, _) => {
            // get connection from pool database
            this.poolRO.getConnection((err, connection) => {
                // if got an error, log the error message & return null
                // so the code doesnt break
                if (err) {
                    console.log(err);
                    resolve(null);
                }
    
                if (connection) {
                    // sqlQuery is query that need to execute,
                    // while values are substitute values that will replace all of parameters
                    connection.query(sqlQuery, values, (err, results, _) => {
                        // release connection after executed query
                        connection.release();

                        // if error, log the message and return null
                        if (err) {
                            resolve(null);
                        }

                        // return the results
                        resolve(results);
                    });
                }
            });
        })
    }

    async findOneData(sqlQuery, values) {
        let dataResponse = await this.findData(sqlQuery, values);

        if (dataResponse) return dataResponse[0];
        else return dataResponse;
    }

    // this function will be use for DML only (create, update, delete)
    async executeDML(sqlQuery, values) {
        // if error, we will resolve with object.
        // so the code doesnt break
        return await new Promise((resolve, _) => {
            this.poolRW.getConnection((err, connection) => {
                // if error found, log the error message and return success state as false
                if (err) {
                    console.log(err);
                    resolve({error: true, success: false});
                }
    
                if (connection) {
                    connection.query(sqlQuery, values, (err, results, _) => {
                        connection.release();

                        if (err) {
                            resolve({error: true, success: false, code: err.code});
                        }

                        // return success state as true since we dont need to return how much row that get affected
                        resolve({error: false, success: true});
                    });
                }
            });
        })
    }
}

module.exports = SQLDatabaseConnection;