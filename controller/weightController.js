const {WeigthModelData} = require('../model');

module.exports = {
  listWeight: async (req, res, next) => {
    try {
      const { date } = req.query;
      let responseModel;

      if (!date) {
        responseModel = {
          avg_max: 0,
          avg_min: 0,
          avg_diff: 0,
          data: []
        };
  
        let totalMax = 0, totalMin = 0, totalDiff = 0;
  
        let weights = await WeigthModelData.listWeight();
        if (weights) {
          responseModel.data = weights;
          for (let i = 0; i < weights.length; i++) {
            let weight = weights[i];
  
            if (!isNaN(weight.max) && !isNaN(weight.min)) {
              totalMax += weight.max;
              totalMin += weight.min;
              totalDiff += weight.max - weight.min;
            }
          }
  
          responseModel.avg_max = totalMax / weights.length;
          responseModel.avg_min = totalMin / weights.length;
          responseModel.avg_diff = totalDiff / weights.length;
        }
      } else {
        responseModel = {
          date: "",
          max: 0,
          min: 0,
          difference: 0
        };
  
        let weight = await WeigthModelData.findWeightByDate(date);
        if (weight) {
          responseModel.date = weight.date_input;
          
          if (!isNaN(weight.max) && !isNaN(weight.min)) {
            responseModel.max = weight.max;
            responseModel.min = weight.min;
  
            let diff = weight.max - weight.min;
            responseModel.difference = diff;
          }
        }
      }

      res.send(responseModel);
    } catch (err){
      next(err);
    }
  },

  createWeight: async (req, res, next) => {
    try {
      const { date, max, min } = req.body;

      if (!date || isNaN(max) || isNaN(min)) {
        res.status(400).send({message: "Invalid input"});
      } else {
        let weight = await WeigthModelData.insertWeight({date, max, min});

        if (weight.error) res.status(400).send(weight);
        else res.send(weight);
      }
    } catch (err){
      next(err);
    }
  },

  updateWeight: async (req, res, next) => {
    try {
      
      const { date } = req.params;
      const { max, min } = req.body;

      if (!date || isNaN(max) || isNaN(min)) {
        res.status(400).send({message: "Invalid input"});
      } else {
        let weight = await WeigthModelData.updateWeight({date, max, min});
        if (weight.error) res.status(400).send(weight);
        else res.send(weight);
      }
    } catch (err){
      next(err);
    }
  }
}