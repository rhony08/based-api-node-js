const chai = require('chai');

const { expect } = chai;
const supertest = require('supertest');

const sinon = require('sinon');
const app = require('../../index');

const api = supertest(app);
const {WeigthModelData} = require('../../model');

describe('API Weight Test', function () {

    const inputCreate = {"date": "2022-05-25", "max": 54, "min": "50"};
    const inputInvalidMaxMinCreate = {"date": "2022-05-25", "max": "a", "min": "50"};
    const inputInvalidDateCreate = {"date": "2022-05", "max": "30", "min": "50"};
    const inputEdit = {"max": 57, "min": "51"};
    const inputInvalidEdit = {"max": "a", "min": "51"};

    const responseCreateUpdate = {
        "error": false,
        "success": true,
        "message": "Success created weight"
    };

    const responseFailedUpdateInvalidDate = {
        "error": true,
        "success": false,
        "code": "ER_TRUNCATED_WRONG_VALUE"
    };

    const responseFailedCreateInvalidDate = {
        "error": true,
        "success": false,
        "message": "Failed to created weight. Please retry."
    };

    const responseWeight = {
        "date": "2022-05-30T00:00:00.000Z",
        "max": 57,
        "min": 51,
        "difference": 6
    };

    const responseWeightByDatabase = {
        "date_input": "2022-05-30T00:00:00.000Z",
        "max": 57,
        "min": 51
    };

    const responseWeights = {
        "avg_max": 55.333333333333336,
        "avg_min": 51.666666666666664,
        "avg_diff": 3.6666666666666665,
        "data": [
            {
                "date": "2022-05-30T00:00:00.000Z",
                "max": 57,
                "min": 51
            },
            {
                "date": "2022-05-26T00:00:00.000Z",
                "max": 55,
                "min": 54
            },
            {
                "date": "2022-05-25T00:00:00.000Z",
                "max": 54,
                "min": 50
            }
        ]
    };

    it('Add Weight - success', (done) => {
        sinon.stub(WeigthModelData, 'insertWeight').resolves(responseCreateUpdate);
        api.post('/weight')
          .set({'Content-Type': 'application/json'})
          .send(inputCreate)
          .end((err, res) => {
            WeigthModelData.insertWeight.restore();
            expect(res.statusCode).to.equal(200);
            expect(err).to.equals(null);
            done();
          });
    });

    it('Add Weight - failed - invalid max/min', (done) => {
        api.post('/weight')
          .set({'Content-Type': 'application/json'})
          .send(inputInvalidMaxMinCreate)
          .end((err, res) => {
            expect(res.statusCode).to.equal(400);
            expect(res.body.message).to.equals("Invalid input");
            done();
          });
    });

    it('Add Weight - failed - invalid date', (done) => {
        sinon.stub(WeigthModelData, 'insertWeight').resolves(responseFailedCreateInvalidDate);
        api.post('/weight')
          .set({'Content-Type': 'application/json'})
          .send(inputInvalidDateCreate)
          .end((err, res) => {
            WeigthModelData.insertWeight.restore();
            expect(res.statusCode).to.equal(400);
            expect(JSON.stringify(res.body)).to.equals(JSON.stringify(responseFailedCreateInvalidDate));
            done();
          });
    });

    it('Edit Weight - success', (done) => {
        sinon.stub(WeigthModelData, 'updateWeight').resolves(responseCreateUpdate);
        api.put('/weight/2022-05-30')
          .set({'Content-Type': 'application/json'})
          .send(inputEdit)
          .end((err, res) => {
            WeigthModelData.updateWeight.restore();
            expect(res.statusCode).to.equal(200);
            expect(err).to.equals(null);
            done();
          });
    });

    it('Edit Weight - failed - invalid max / min', (done) => {
        api.put('/weight/2022-05')
          .set({'Content-Type': 'application/json'})
          .send(inputInvalidEdit)
          .end((err, res) => {
            expect(res.statusCode).to.equal(400);
            expect(res.body.message).to.equals("Invalid input");
            done();
          });
    });

    it('Edit Weight - failed - invalid date', (done) => {
        sinon.stub(WeigthModelData, 'updateWeight').resolves(responseFailedUpdateInvalidDate);
        api.put('/weight/2022-05')
          .set({'Content-Type': 'application/json'})
          .send(inputEdit)
          .end((err, res) => {
            WeigthModelData.updateWeight.restore();
            expect(res.statusCode).to.equal(400);
            expect(JSON.stringify(res.body)).to.equals(JSON.stringify(responseFailedUpdateInvalidDate));
            done();
          });
    });

    it('Get All Weights', (done) => {
        sinon.stub(WeigthModelData, 'listWeight').resolves(responseWeights.data);
        api.get('/weight')
          .end((err, res) => {
            WeigthModelData.listWeight.restore();
            expect(res.statusCode).to.equal(200);
            expect(JSON.stringify(res.body)).to.equals(JSON.stringify(responseWeights));
            done();
          });
    });

    it('Get Weight by date', (done) => {
        sinon.stub(WeigthModelData, 'findWeightByDate').resolves(responseWeightByDatabase);
        api.get('/weight?date=2022-05-30')
          .end((err, res) => {
            WeigthModelData.findWeightByDate.restore();
            expect(res.statusCode).to.equal(200);
            expect(JSON.stringify(res.body)).to.equals(JSON.stringify(responseWeight));
            done();
          });
    });
});