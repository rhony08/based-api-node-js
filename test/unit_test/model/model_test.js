const chai = require('chai');

const { expect } = chai;

const sinon = require('sinon');

const weight = require('../../../model/weight').WeigthModelData;

class Connection {
    findOneData(sqlQuery, params) {
        return {};
    }

    findData(sqlQuery, params) {
        return [];
    }

    executeDML(sqlQuery, params) {
        return {
            error: false,
            success: true,
            code: "",
        };
    }
}

describe('Weight Model - Unit Test', function () {

    const weightModel = new weight(new Connection());

    it('Get Weight List', async() => {
        const resp = [{
            'field_1': 'test',
            'field_2': 'test test'
        }];
        sinon.stub(Connection.prototype, 'findData').resolves(resp);

        const data = await weightModel.listWeight();

        expect(data.length).to.equals(1);
        expect(JSON.stringify(data)).to.equals(JSON.stringify(resp));

        Connection.prototype.findData.restore();
    });

    it('Get Weight by date', async() => {
        const resp = {
            'field_1': 'test',
            'field_2': 'test test'
        };
        sinon.stub(Connection.prototype, 'findOneData').resolves(resp);

        const data = await weightModel.findWeightByDate();

        expect(JSON.stringify(data)).to.equals(JSON.stringify(resp));

        Connection.prototype.findOneData.restore();
    });

    it('Insert Weight - success', async() => {
        const resp = {
            "error": false,
            "success": true,
            "code": ""
        };

        sinon.stub(Connection.prototype, 'executeDML').resolves(resp);

        const data = await weightModel.insertWeight({date: "2022-05-30", max: 10, min: 1});

        expect(JSON.stringify(data)).to.equals(JSON.stringify(resp));

        Connection.prototype.executeDML.restore();
    });

    it('Insert Weight - failed - duplicated date', async() => {
        const resp = {
            "error": true,
            "success": false,
            "code": "ER_DUP_ENTRY"
        };

        const responseData = {
            "error": true,
            "success": false,
            "message": "Data on that date already registered."
        };

        sinon.stub(Connection.prototype, 'executeDML').resolves(resp);

        const data = await weightModel.insertWeight({date: "2022-05-30", max: 10, min: 1});

        expect(JSON.stringify(data)).to.equals(JSON.stringify(responseData));

        Connection.prototype.executeDML.restore();
    });

    it('Insert Weight - failed - other error', async() => {
        const resp = {
            "error": true,
            "success": false,
            "code": "ER_TRUNCATED_WRONG_VALUE"
        };

        const responseData = {
            "error": true,
            "success": false,
            "message": "Failed to created weight. Please retry."
        };

        sinon.stub(Connection.prototype, 'executeDML').resolves(resp);

        const data = await weightModel.insertWeight({date: "2022-05-30", max: 10, min: 1});

        expect(JSON.stringify(data)).to.equals(JSON.stringify(responseData));

        Connection.prototype.executeDML.restore();
    });

    it('Update Weight - success', async() => {
        const resp = {
            "error": false,
            "success": true,
            "code": ""
        };

        const responseData = {
            "error": false,
            "success": true,
            "code": "",
            "message": "Success updated weight"
        };

        sinon.stub(Connection.prototype, 'executeDML').resolves(resp);

        const data = await weightModel.updateWeight({date: "2022-05-30", max: 10, min: 1});

        expect(JSON.stringify(data)).to.equals(JSON.stringify(responseData));

        Connection.prototype.executeDML.restore();
    });

    it('Insert Weight - failed', async() => {
        const resp = {
            "error": true,
            "success": false,
            "code": "ER_TRUNCATED_WRONG_VALUE"
        };

        sinon.stub(Connection.prototype, 'executeDML').resolves(resp);

        const data = await weightModel.updateWeight({date: "2022-05-30", max: 10, min: 1});

        expect(JSON.stringify(data)).to.equals(JSON.stringify(data));

        Connection.prototype.executeDML.restore();
    });
});