const chai = require('chai');

const { expect } = chai;

const axios = require('axios');

const host = "http://localhost:8001";

describe('Integration Weight Test', function () {

    const date = "2022-06-20";
    const invalidDate = "2022-06";

    const inputCreate = {"date": date, "max": 54, "min": "50"};
    const inputInvalidMaxMinCreate = {"date": date, "max": "a", "min": "50"};
    const inputInvalidDateCreate = {"date": invalidDate, "max": "30", "min": "50"};
    const inputEdit = {"max": "57", "min": "51"};
    const inputInvalidEdit = {"max": "a", "min": "51"};

    const responseCreate = {
        "error": false,
        "success": true,
        "message": "Success created weight"
    };

    const responseUpdate = {
      "error": false,
      "success": true,
      "message": "Success updated weight"
  };

    const responseFailedUpdateInvalidDate = {
        "error": true,
        "success": false,
        "code": "ER_TRUNCATED_WRONG_VALUE"
    };

    const responseFailedCreateInvalidDate = {
        "error": true,
        "success": false,
        "message": "Failed to created weight. Please retry."
    };

    /**
     * only can for first execution,
     * need delete API so all integration test can run continuosly.
     * please comment this after first runner.
     * 
     * */ 
    it('Add Weight - success', async() => {
      try {
        const resp = await axios.post(`${host}/weight`, inputCreate);
        expect(resp.status).to.equal(200);
        expect(JSON.stringify(resp.data)).to.equals(JSON.stringify(responseCreate));
      } catch (err) {
        expect(err).to.equal(null);
      }
    });

    it('Add Weight - failed - invalid max/min', async () => {
      try {
        const resp = await axios.post(`${host}/weight`, inputInvalidMaxMinCreate);
      } catch (err) {
        expect(err.response.status).to.equal(400);
        expect(err.response.data.message).to.equals("Invalid input");
      }
    });

    it('Add Weight - failed - invalid date', async () => {
      try {
        const resp = await axios.post(`${host}/weight`, inputInvalidDateCreate);
      } catch (err) {
        expect(err.response.status).to.equal(400);
        expect(JSON.stringify(err.response.data)).to.equals(JSON.stringify(responseFailedCreateInvalidDate));
      }
    });

    it('Edit Weight - success', async () => {
      try {
        const response = await axios.put(`${host}/weight/${date}`, inputEdit);
        expect(response.status).to.equal(200);
        expect(JSON.stringify(response.data)).to.equals(JSON.stringify(responseUpdate));
      } catch (err) {
        expect(err).to.equal(null);
      }
    });

    it('Edit Weight - failed - invalid max / min', async () => {
      try {
        const resp = await axios.put(`${host}/weight/${date}`, inputInvalidEdit);
      } catch (err) {
        expect(err.response.status).to.equal(400);
        expect(JSON.stringify(err.response.data)).to.equals(JSON.stringify({message: 'Invalid input'}));
      }
    });

    it('Edit Weight - failed - invalid date', async() => {
      try {
        const resp = await axios.put(`${host}/weight/${invalidDate}`, inputEdit);
      } catch (err) {
        expect(err.response.status).to.equal(400);
        expect(JSON.stringify(err.response.data)).to.equals(JSON.stringify(responseFailedUpdateInvalidDate));
      }
    });

    it('Get All Weights', async () => {

      const res = await axios.get(`${host}/weight`);

      let totalMax = 0, totalMin = 0, totalDiff = 0;
      for (let idx = 0; idx < res.data.data.length; idx++) {
        const weight = res.data.data[idx];
        totalMax += weight.max;
        totalMin += weight.min;
        totalDiff += weight.max - weight.min;
      }

      expect(res.status).to.equal(200);
      expect(res.data.data.length).to.greaterThan(0);

      expect((totalMax / res.data.data.length)).to.equals(res.data.avg_max);
      expect((totalMin / res.data.data.length)).to.equals(res.data.avg_min);
      expect((totalDiff / res.data.data.length)).to.equals(res.data.avg_diff);
    });

    it('Get Weight by date', async() => {
      const res = await axios.get(`${host}/weight?date=${date}`);
      expect(res.status).to.equal(200);
      expect(JSON.stringify(res.data.max)).to.equal(inputEdit.max); // get latest value from update data
      expect(JSON.stringify(res.data.min)).to.equal(inputEdit.min); // get latest value from update data
    });
});