const {SQLConnection} = require('../database');

// set database connection
const configRO = {
  limit : 4,
  host: process.env.DATABASE_RO_HOST,
  user: process.env.DATABASE_RO_USER,
  password: process.env.DATABASE_RO_PWD,
  database: process.env.DATABASE_RO_NAME,
  port: process.env.DATABASE_RO_PORT,
  type: "RO"
};

const configRW = {
  limit : 2,
  host: process.env.DATABASE_HOST,
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_PWD,
  database: process.env.DATABASE_NAME,
  port: process.env.DATABASE_PORT,
  type: "RW"
};

const dbConnection = new SQLConnection();
dbConnection.setConnection(configRO.host, configRO.user, configRO.password,
  configRO.database, configRO.limit, configRO.type);
dbConnection.setConnection(configRW.host, configRW.user, configRW.password,
  configRW.database, configRW.limit, configRW.type);

const Weight = require('./weight').WeigthModelData;
const WeigthModelData = new Weight(dbConnection);

module.exports = {
  WeigthModelData
};