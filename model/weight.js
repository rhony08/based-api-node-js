class WeigthModelData {
  constructor(connection) {
    this.conn = connection;
  }

  async findWeightByDate(date) {
    const sql = 'SELECT date_input, max, min FROM weights WHERE date_input = ?';
    
    let resultData = await this.conn.findOneData(sql, [date]);

    return resultData;
  }

  async listWeight() {
    const sql = 'SELECT date_input as date, max, min FROM weights ORDER BY date_input desc';
    
    let resultData = await this.conn.findData(sql, []);

    return resultData;
  }

  async insertWeight(data) {
    const sql = 'INSERT INTO weights (date_input, max, min) VALUES (?, ?, ?)';
    
    let resultData = await this.conn.executeDML(sql, [data.date, data.max, data.min]);
    if (!resultData.error) {
      resultData.message = "Success created weight";
    } else {
      if (resultData.code) {
        let errorMessage;

        if (resultData.code === 'ER_DUP_ENTRY') {
          errorMessage = 'Data on that date already registered.';
        } else {
          errorMessage = 'Failed to created weight. Please retry.';
        }

        delete resultData.code; // remove code, since we dont need to return this error

        resultData.message = errorMessage;
      }
    }

    return resultData;
  }

  async updateWeight(inputData) {
    const sql = 'UPDATE weights SET max = ?, min = ? WHERE date_input = ?';
    
    let resultData = await this.conn.executeDML(sql, [inputData.max, inputData.min, inputData.date]);
    if (!resultData.error) {
      resultData.message = "Success updated weight";
    }

    return resultData;
  }
}

module.exports = {WeigthModelData};
