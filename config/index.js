require('dotenv').config();

module.exports = {
  PORT: process.env.NODE_DOCKER_PORT || 8080
};