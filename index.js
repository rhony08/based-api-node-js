const express = require('express');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const http = require('http');

const cors = require('cors');

// config keys
const config = require('./config');

const Router = require('./router');

const PORT = config.PORT || 8001;

const app = express();

process.on('unhandledRejection', err => {});

// safety first
app.use(helmet());

// CORS Dev
const corsOptions = {
  origin: true,
  credentials: true
};

app.use(cors(corsOptions));
app.use(bodyParser.json());

// our server instance
const server = http.createServer(app)

Router(app);

app.use(( error , req, res, next) => {
  return res.status(error.code || 422).send({ 
    status: {
      status: error.code || 422,
      message: error.message,
      succeeded: false
    }});
});

server.listen(PORT, () => {
  console.log(`Listen on port ${PORT}`)
})

module.exports = app;